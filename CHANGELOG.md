Change Log
==========

Current Version: 1.1.0

1.1.0
-----
* Added Brick Pyramids (Idea from Minecraft Infdev)
* Added Graveyards

1.0.1
------
* Added Game Compatibilty Tags

1.0.0
-----
* Added Desert Oasis
* Added Birch Forest Ruins