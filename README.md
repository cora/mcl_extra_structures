mcl_extra_structures
====================
Adds extra structures to the MineClone2 game. Includes structures based on YouTube videos about what could be in future Minecraft updates. This will hopefully be continuously updated and expanded.

Structure List
--------------
* Oasis
* Birch Forest Temple Ruins
* Brick Pyramid
* Graveyard